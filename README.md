# buildfpc

This is a bash script to compile the FreePascal source 
(see https://gitlab.com/freepascal.org/fpc/source) 
for several CPU/OS targets.

Originally used to demonstrate a building issue 
(https://gitlab.com/freepascal.org/fpc/source/-/issues/39930).

The issue was between the keyboard and the chair. 
Screwed up the install directory somehow 
while pointing to it 
- in the `$PATH` and
- in the `~/.fpc.cfg` file.

This repository includes the build log for reference.
