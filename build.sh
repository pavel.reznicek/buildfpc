#!/bin/bash

OLDFPC=/usr/bin/ppcx64
NEWFPC="$PWD/compiler/ppcx64"
PREFIX=~/.local
BUILDLOG='build.log'

# Set up the English environment
LC_ALL="en_US.UTF-8"
LANG="en_US.UTF-8"
LANGUAGE="en_US:en"

function msg()
{
    local SEP='------------------------------------------------------'
    echo        | tee -a "$BUILDLOG"
    echo "$SEP" | tee -a "$BUILDLOG"
    echo "$1"   | tee -a "$BUILDLOG"
    echo "$SEP" | tee -a "$BUILDLOG"
    echo        | tee -a "$BUILDLOG"
}

function mkcommand()
{
    local ACTION="$1"
    local CPU="$2"
    local OS="$3"
    local COMMANDBASE="$ACTION CPU_TARGET=\"$CPU\" OS_TARGET=\"$OS\" FPC=\"$OLDFPC\" -j `nproc`"
    if ! [[ "$OS" = 'atari' ]]
    then local COMMAND="$COMMANDBASE 2>&1"
    else local COMMAND="$COMMANDBASE CROSSOPT=\"-XV -Avasm -Cp68000\" 2>&1"
    fi
    echo "$COMMAND"
}

function mk()
{
    local ACTION="$1"
    local CPU="$2"
    local OS="$3"
    local TGT="$CPU-$OS"
    local TGTLOG="build.$TGT.log"
    local COMMAND=`mkcommand "$ACTION" "$CPU" "$OS"`
    echo "Command: $COMMAND" | tee -a "$BUILDLOG"
    eval "$COMMAND" | tee -a "$TGTLOG"
    local PIPERESULT="${PIPESTATUS[0]}"
    if [[ $PIPERESULT == 0 ]]
    then {
        echo '...'  >> "$BUILDLOG"
        echo '(OK)' >> "$BUILDLOG"
    }
    else cat "$TGTLOG" >> "$BUILDLOG"
    fi
}

function clean()
{
    local TGTLOG="build.clean.log"
    msg "Running make clean..."
    make clean 2>&1 | tee -a "$TGTLOG"
    local PIPERESULT="${PIPESTATUS[0]}"
    if [[ $PIPERESULT == 0 ]]
    then {
        echo '...'  >> "$BUILDLOG"
        echo '(OK)' >> "$BUILDLOG"
    }
    else cat "$TGTLOG" >> "$BUILDLOG"
    fi
}

function build()
{
    local ACTION="make all"
    local CPU="$1"
    local OS="$2"
    local TGT="$CPU-$OS"
    msg "Compiling for $TGT..."
    mk "$ACTION" "$CPU" "$OS"
}

function inst()
{
    local ACTION="make install crossinstall INSTALL_PREFIX=\"$PREFIX\""
    local CPU="$1"
    local OS="$2"
    local TGT="$CPU-$OS"
    msg "Installing for $TGT..."
    mk "$ACTION" "$CPU" "$OS"
}

function mkcfg()
{
    cd "$PREFIX/lib/fpc" &&
    mkdir -p etc &&
    fpcmkcfg -d basepath="$PREFIX/lib/fpc/\$fpcversion" -o etc/fpc.cfg
}

echo "Compiling FPC..." | tee "$BUILDLOG"

#clean

msg "Running fpcmake -Tall..."
fpcmake -Tall | tee -a "$BUILDLOG"

build x86_64  linux
build i386    linux
build x86_64  win64
build i386    win32
build aarch64 linux
build arm     linux
build m68k    atari

inst x86_64 linux
inst i386    linux
inst x86_64  win64
inst i386    win32
inst aarch64 linux
inst arm     linux
inst m68k    atari

msg "Creating a config file..."
mkcfg | tee -a "$BUILDLOG"
